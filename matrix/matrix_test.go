package matrix

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewMatrix(t *testing.T) {
	cases := []struct {
		name     string
		values   []float32
		shape    shape
		expected Matrix
		err      error
	}{
		{
			name:     "create matrix success",
			values:   []float32{1, 2, 3, 4},
			shape:    shape{2, 2},
			expected: Matrix{values: [][]float32{{1, 2}, {3, 4}}},
		},
		{
			name:   "create matrix insufficient values error",
			values: []float32{1, 2, 3},
			shape:  shape{2, 2},
			err:    ErrorCreateMatrix,
		},
		{
			name:   "create matrix exceeded values error",
			values: []float32{1, 2, 3, 4, 5, 6},
			shape:  shape{2, 2},
			err:    ErrorCreateMatrix,
		},
	}
	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			matrix, err := NewMatrix(tc.shape.rows, tc.shape.columns, tc.values)
			if err != nil {
				assert.Equal(t, tc.err, err)
				return
			}
			assert.Equal(t, tc.expected.values, matrix.values)
		})
	}
}

func TestNewShape(t *testing.T) {
	cases := []struct {
		name     string
		rows     int
		columns  int
		expected shape
		err      error
	}{
		{
			name:     "create shape success",
			rows:     2,
			columns:  3,
			expected: shape{2, 3},
		},
		{
			name:    "create shape error, invalid parameter",
			rows:    -1,
			columns: 3,
			err:     ErrorCreateShape,
		},
	}
	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			shape, err := newShape(tc.rows, tc.columns)
			if err != nil {
				assert.Equal(t, tc.err, err)
				return
			}
			assert.Equal(t, tc.expected, shape)
		})
	}
}

func TestReshape(t *testing.T) {
	values := []float32{1, 2, 3, 4, 5, 6}
	matrix, err := NewMatrix(3, 2, values)
	assert.Nil(t, err)
	assert.Equal(t, matrix.values, [][]float32{{1, 2}, {3, 4}, {5, 6}})
	t.Run("Reshape success", func(t *testing.T) {
		err := matrix.ReShape(2, 3)
		assert.Nil(t, err)
		expectedMatrix := Matrix{values: [][]float32{{1, 2, 3}, {4, 5, 6}}}
		assert.Equal(t, expectedMatrix.values, matrix.values)
	})
	t.Run("Reshape error", func(t *testing.T) {
		err := matrix.ReShape(5, 3)
		assert.Equal(t, ErrorCreateMatrix, err)
	})
}

func TestOperations(t *testing.T) {
	values := []float32{1, 2, 3, 4}
	matrix, err := NewMatrix(2, 2, values)
	assert.Nil(t, err)
	assert.Equal(t, matrix.values, [][]float32{{1, 2}, {3, 4}})
	t.Run("Addition", func(t *testing.T) {
		matrix.Addition(2)
		assert.Equal(t, [][]float32{{3, 4}, {5, 6}}, matrix.values)
	})
	t.Run("Subtraction", func(t *testing.T) {
		matrix.Subtraction(2)
		assert.Equal(t, [][]float32{{1, 2}, {3, 4}}, matrix.values)
	})
	t.Run("Division", func(t *testing.T) {
		err := matrix.Division(1)
		assert.Nil(t, err)
		assert.Equal(t, [][]float32{{1, 2}, {3, 4}}, matrix.values)
	})
	t.Run("Multiply", func(t *testing.T) {
		matrix.Multiply(2)
		assert.Equal(t, [][]float32{{2, 4}, {6, 8}}, matrix.values)
	})
	t.Run("Division error", func(t *testing.T) {
		err := matrix.Division(0)
		assert.Equal(t, ErrorDivission, err)
	})
}
func TestIndexSelect(t *testing.T) {
	values := []float32{1, 2, 3}
	matrix, err := NewMatrix(1, 3, values)
	assert.Nil(t, err)
	t.Run("Index select success", func(t *testing.T) {
		found, err := matrix.IndexSelect(0, 2)
		assert.Nil(t, err)
		assert.Equal(t, float32(3), found)
	})
	t.Run("Index select error", func(t *testing.T) {
		found, err := matrix.IndexSelect(0, 5)
		assert.Equal(t, err, ErrorIndexSelect)
		assert.Equal(t, float32(0), found)
	})
}

func TestCustomIndexSelect(t *testing.T) {
	values := []float32{1, 2, 3, 4}
	matrix, err := NewMatrix(1, 4, values)
	assert.Nil(t, err)
	t.Run("Index select success", func(t *testing.T) {
		found, err := CustomIndexSelect(matrix, 0, []int{0, 0, 2})
		assert.Nil(t, err)
		assert.Equal(t, []float32{1, 1, 3}, found)
	})
	t.Run("Index select error", func(t *testing.T) {
		_, err := CustomIndexSelect(matrix, 0, []int{5})
		assert.Equal(t, ErrorIndexSelect, err)
	})
	t.Run("Index select success", func(t *testing.T) {
		matrix.ReShape(2, 2)
		found, err := CustomIndexSelect(matrix, 0, []int{0})
		assert.Nil(t, err)
		assert.Equal(t, [][]float32{{1, 2}}, found)
	})
	t.Run("Index select success", func(t *testing.T) {
		found, err := CustomIndexSelect(matrix, 0, []int{0, 0, 1})
		assert.Nil(t, err)
		assert.Equal(t, [][]float32{{1, 2}, {1, 2}, {3, 4}}, found)
	})
	t.Run("Index select success", func(t *testing.T) {
		found, err := CustomIndexSelect(matrix, 1, []int{0, 0})
		assert.Nil(t, err)
		assert.Equal(t, [][]float32{{1, 1}, {3, 3}}, found)
	})
	t.Run("Index select success", func(t *testing.T) {
		found, err := CustomIndexSelect(matrix, 1, []int{0, 0, 1, 1})
		assert.Nil(t, err)
		assert.Equal(t, [][]float32{{1, 1, 2, 2}, {3, 3, 4, 4}}, found)
	})
	t.Run("Index select error", func(t *testing.T) {
		_, err := CustomIndexSelect(matrix, 0, []int{8})
		assert.Equal(t, ErrorIndexSelect, err)
	})
	t.Run("Index select error", func(t *testing.T) {
		_, err := CustomIndexSelect(matrix, 1, []int{8})
		assert.Equal(t, ErrorIndexSelect, err)
	})
}
