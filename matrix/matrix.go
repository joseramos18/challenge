package matrix

import (
	"errors"
)

var (
	ErrorCreateShape  = errors.New("Invalid parameters")
	ErrorCreateMatrix = errors.New("Values quantity invalid")
	ErrorIndexSelect  = errors.New("Index select not allowed")
	ErrorDivission    = errors.New("Invalid parameter")
)

type Matrix struct {
	numbers []float32
	shape   shape
	values  [][]float32
}

type shape struct {
	rows, columns int
}

func newShape(rows, columns int) (shape, error) {
	if rows < 0 || columns < 0 {
		return shape{}, ErrorCreateShape
	}
	return shape{rows, columns}, nil
}

func NewMatrix(rows, columns int, values []float32) (Matrix, error) {
	var m Matrix
	var index int
	shape, err := newShape(rows, columns)
	if err != nil {
		return m, err
	}
	if len(values) != shape.columns*shape.rows {
		return m, ErrorCreateMatrix
	}
	for i := 0; i < shape.rows; i++ {
		elements := make([]float32, shape.columns)
		for i := range elements {
			elements[i] = values[index]
			index++
		}
		m.values = append(m.values, elements)
	}
	m.numbers = values
	m.shape = shape
	return m, nil
}

func (m *Matrix) ReShape(rows, columns int) error {
	shaped, err := NewMatrix(rows, columns, m.numbers)
	if err != nil {
		return err
	}
	*m = shaped
	return nil
}

func (m *Matrix) Addition(product float32) {
	m.resolveOperation(product, "add")
}

func (m *Matrix) Subtraction(product float32) {
	m.resolveOperation(product, "sub")
}

func (m *Matrix) Multiply(product float32) {
	m.resolveOperation(product, "mult")
}

func (m *Matrix) Division(product float32) error {
	if product == 0 {
		return ErrorDivission
	}
	m.resolveOperation(product, "div")
	return nil
}

func (m *Matrix) resolveOperation(product float32, operation string) {
	var newValues []float32
	switch operation {
	case "add":
		for _, value := range m.numbers {
			newValues = append(newValues, value+product)
		}
	case "sub":
		for _, value := range m.numbers {
			newValues = append(newValues, value-product)
		}
	case "mult":
		for _, value := range m.numbers {
			newValues = append(newValues, value*product)
		}
	case "div":
		for _, value := range m.numbers {
			newValues = append(newValues, value/product)
		}
	default:
		newValues = m.numbers
	}
	matrix, _ := NewMatrix(m.shape.rows, m.shape.columns, newValues)
	*m = matrix
}

func (m *Matrix) IndexSelect(row, column int) (float32, error) {
	if m.shape.rows <= row || m.shape.columns <= column {
		return 0, ErrorIndexSelect
	}
	return m.values[row][column], nil
}

func CustomIndexSelect(m Matrix, direction int, indexes []int) (interface{}, error) {
	if m.shape.rows == 1 {
		var valuesFound []float32
		for _, i := range indexes {
			found, err := m.IndexSelect(0, i)
			if err != nil {
				return nil, ErrorIndexSelect
			}
			valuesFound = append(valuesFound, found)
		}
		return valuesFound, nil
	}
	var valuesFound [][]float32
	if direction == 0 {
		for _, i := range indexes {
			if m.shape.rows <= i {
				return nil, ErrorIndexSelect
			}
			valuesFound = append(valuesFound, m.values[i])
		}
	} else {
		for indexRow := range m.values {
			var foundByRow []float32
			for _, i := range indexes {
				found, err := m.IndexSelect(indexRow, i)
				if err != nil {
					return nil, ErrorIndexSelect
				}
				foundByRow = append(foundByRow, found)
			}
			valuesFound = append(valuesFound, foundByRow)
		}
	}
	return valuesFound, nil
}
