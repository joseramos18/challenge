package main

import (
	"challenge/matrix"
	"fmt"
)

func main() {
	matrix, err := run()
	if err != nil {
		fmt.Println("error creating matrix: ", err.Error())
	}
	fmt.Println(matrix)
}

func run() (matrix.Matrix, error) {
	values := []float32{1, 2, 3, 4}
	m, err := matrix.NewMatrix(1, 4, values)
	if err != nil {
		return matrix.Matrix{}, nil
	}
	return m, nil
}
